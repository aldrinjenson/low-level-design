// const { test, expect } = require("jest");
const { Schedule } = require(".");
const { Room } = require("./Services/Room");

test('calculate function works correctly', () => {
  const scheduler = new Schedule({ attendees: ['1', '2', '3', '4', '5'], interviewers: ['A', 'B', 'C'], rooms: [new Room('R1'), new Room('R2')] })
  expect(
    scheduler.calculateSchedule()
  ).toEqual(
    [
      { attendee: '1', interviwer: 'A', room: 'R1', time: '9 - 11' },
      { attendee: '2', interviwer: 'B', room: 'R2', time: '9 - 11' },
      { attendee: '3', interviwer: 'A', room: 'R1', time: '11 - 1' },
      { attendee: '4', interviwer: 'B', room: 'R2', time: '11 - 1' },
      { attendee: '5', interviwer: 'A', room: 'R1', time: '1 - 4' }
    ]
  )
})