const BREAK_DURATION = 1
const INTERVIEW_DURATION = 2
const START_TIME = 9
const BREAK_START_HOUR = 14

class Room {
  currTime = START_TIME;
  constructor(roomName = '') {
    this.roomName = roomName
  }
  getName = () => {
    return this.roomName
  }
  getCurrTime = () => {
    return this.currTime
  }

  doMeetingAndBreak = () => {
    if (this.currTime === BREAK_START_HOUR - 1 || this.currTime === BREAK_START_HOUR) {
      this.currTime += BREAK_DURATION
    }
    this.currTime += INTERVIEW_DURATION
  }
}

module.exports = { Room }