const { Room } = require("./Services/Room")

const MEETING_END_TIME = 18

class Schedule {
  constructor({ attendees, interviewers, rooms }) {
    this.attendees = attendees
    this.interviewers = interviewers
    this.rooms = rooms
  }

  calculateSchedule = () => {
    const limitingValue = Math.min(this.interviewers.length, this.rooms.length)
    // console.log({ attendees: this.attendees, interviewers: this.interviewers, rooms: this.rooms.map(r => r.getName()) });
    // console.log('Attendee\tInterviewer\tRoom\tTime');

    const schedule = []
    let i, j
    let timeOver = false
    for (i = 0; !timeOver && i < this.attendees.length; i++) {
      for (j = i; !timeOver && j < limitingValue + i && j < this.attendees.length; j++) {
        const attendee = this.attendees[j]
        const matchedRoom = this.rooms[j % limitingValue]
        const startTime = Math.floor((matchedRoom.getCurrTime()) % 12)
        matchedRoom.doMeetingAndBreak()
        const endTime = Math.floor((matchedRoom.getCurrTime()) % 12)
        if (matchedRoom.getCurrTime() > MEETING_END_TIME) {
          console.log(attendee + ' will be having meeting tomorrow');
          timeOver = true
          break;
        }
        const obj = {
          attendee,
          interviwer: this.interviewers[j % limitingValue],
          room: matchedRoom.getName(),
          time: `${startTime} - ${endTime}`
        }
        schedule.push(obj)
      }
      i = j - 1
    }
    console.log(schedule);
    // return schedule
  }

}

new Schedule({ attendees: ['1', '2', '3', '4', '5'], interviewers: ['A', 'B', 'C'], rooms: [new Room('R1'), new Room('R2')] }).calculateSchedule()
new Schedule({ attendees: ['1', '2', '3', '4', '5'], interviewers: ['A', 'B', 'C'], rooms: [new Room('R1'), new Room('R2'), new Room('R3')] }).calculateSchedule()
new Schedule({ attendees: ['1', '2', '3', '4', '5', '6', '7'], interviewers: ['A', 'B'], rooms: [new Room('R1'), new Room('R2')] }).calculateSchedule()
new Schedule({ attendees: ['1', '2', '3', '4', '5'], interviewers: ['A', 'B'], rooms: [new Room('R1'), new Room('R2'), new Room('R3')] }).calculateSchedule()
new Schedule({ attendees: ['1', '2', '3', '4', '5'], interviewers: ['A'], rooms: [new Room('R1'), new Room('R2')] }).calculateSchedule()


module.exports = {
  Schedule
}