class Customer {
  constructor({ name, phoneNo, email, address }) {
    this.name = name
    this.phoneNo = phoneNo
    this.email = email
    this.address = address
  }

}

module.exports = Customer