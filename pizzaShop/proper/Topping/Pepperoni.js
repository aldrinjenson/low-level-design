const Topping = require("./Topping")

class Pepperoni extends Topping {
  #name = "Pepperoni"
  #cost = 45

  getCost() {
    return this.#cost
  }
}

module.exports = Pepperoni