const MozarellaCheese = require("./MozarellaCheese")
const Pepperoni = require("./Pepperoni")

class ToppingFactory {
  create(toppingName = '') {
    if (toppingName == 'Mozzarella Cheese') {
      return new MozarellaCheese()
    } else if (toppingName === 'Pepperoni') {
      return new Pepperoni()
    }
  }
}

module.exports = { ToppingFactory: new ToppingFactory() }
