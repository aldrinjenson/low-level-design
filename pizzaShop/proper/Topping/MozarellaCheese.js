const Topping = require("./Topping")

class MozarellaCheese extends Topping {
  #name = "Mozarella Cheese"
  #cost = 30

  getCost() {
    return this.#cost
  }
}

module.exports = MozarellaCheese