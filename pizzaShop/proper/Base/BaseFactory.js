const Regular = require("./Regular")
const WholeWheat = require("./WholeWheat")

class BaseFactory {
  create(baseName = '') {
    if (baseName == 'Whole Wheat') {
      return new WholeWheat()
    } else if (baseName === 'Regular') {
      return new Regular()
    }
  }
}

module.exports = { BaseFactory: new BaseFactory() }
