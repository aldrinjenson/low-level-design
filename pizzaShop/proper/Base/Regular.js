const Base = require("./Base")

class Regular extends Base {
  #name = "Regular"
  #cost = 100

  getCost() {
    return this.#cost
  }
}
module.exports = Regular