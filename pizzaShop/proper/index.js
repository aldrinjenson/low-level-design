const { BaseFactory } = require("./Base/BaseFactory")
const { ToppingFactory } = require("./Topping/ToppingFactory")

class PizzaOrder {
  constructor({ base, sauce, toppings, drinks, desserts }) {
    this.base = BaseFactory.create(base)
    this.toppings = toppings.map(tp =>
      ToppingFactory.create(tp)
    )
    this.cost = 0
  }

  calculateDiscount() {
    if (this.drinks.length && this.desserts.length) {
      this.cost -= this.cost * 5 / 100
    }
  }


  calculateCost() {
    console.log(this.base, this.toppings);
    this.cost += this.base.getCost()
    for (const tp of this.toppings) {
      this.cost += tp.getCost()
    }
    return this.cost
  }
}

const p = new PizzaOrder({ base: 'Whole Wheat', sauce: 'Marinara Sauce', toppings: ['Mozzarella Cheese', 'Pepperoni'], drinks: ['Coke'], desserts: ['Lava Cake'] })
console.log("Cost = " + p.calculateCost())