const baseItems = {
  "Regular": { name: "Regular", price: 50, },
  "Whole Wheat": { name: "Whole wheat", price: 75, },
}
const sauceItems = {
  "Marinara sauce": { name: "Regular" },
  "Pesto sauce": { name: "Pesto sauce" },
}
const toppingItems = {
  "Mozzarella cheese": { name: "Mozzarella cheese", price: 30 },
  "Cheddar cheese": { name: "Cheddar cheese", price: 35 },
  "Spinach": { name: "Spinach", price: 20 },
  "Corn": { name: "Corn", price: 15 },
  "Mushroom": { name: "Mushroom", price: 15 },
  "Chicken": { name: "Chicken", price: 50 },
  "Pepperoni": { name: "Pepperoni", price: 45 },
  "Pepsi Drinks": { name: "Pepsi Drinks", price: 17 },
}
const addOns = {
  "7 - up": { name: "7 - up Drinks", type: ' Drinks', price: 19 },
  "Coke": { name: "Coke Drinks", type: ' Drinks":', price: 20 },
  "Lava Cake": { name: "Lava cake Dessert", type: ' Dessert', price: 95 },
  "Chocolate brownie": { name: "Chocolate brownie Dessert", type: ' Dessert', price: 86 },
}


class PizzaShop {
  cost = 0
  receipt = ''
  constructor({ base, sauce, toppings, drinks, desserts }) {
    this.base = base,
      this.sauce = sauce,
      this.toppings = toppings,
      this.drinks = drinks,
      this.desserts = desserts
  }

  applyDiscount() {
    if (this.drinks.length && this.desserts.length) {
      this.cost -= this.cost * 5 / 100
    }
  }

  calculateCost() {
    const baseEl = baseItems[this.base]
    this.cost += baseEl.price
    this.receipt += `${baseEl.name}(${baseEl.price}Rs) added\n`
    this.receipt += this.sauce + ' added\n'
    for (const topping of this.toppings) {
      const toppingEl = toppingItems[topping]
      this.receipt += `${toppingEl.name}(${toppingEl.price}Rs) added\n`
      this.cost += toppingEl.price
    }
    for (const drink of this.drinks) {
      const drinkEl = addOns[drink]
      this.receipt += `${drinkEl.name}(${drinkEl.price}Rs) added\n`
      this.cost += drinkEl.price
    }
    for (const dessert of this.desserts) {
      const dessertEl = addOns[dessert]
      this.receipt += `${dessertEl.name}(${dessertEl.price}Rs) added\n`
      this.cost += dessertEl.price
    }
    console.log(this.receipt)

    this.applyDiscount()
    return this.cost
  }
}

const p = new PizzaShop({ base: 'Whole Wheat', sauce: 'Marinara Sauce', toppings: ['Mozzarella cheese', 'Pepperoni'], drinks: ['Coke'], desserts: ['Lava Cake'] })
// console.log(p.calculateCost() === 251.75)
console.log(p.calculateCost())
