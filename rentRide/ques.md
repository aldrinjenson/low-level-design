##  Rent a Ride
As a customer to Rent a Ride you book a cab. 
We **charge you as per the distance covered.**
We charge **8rs/km.**
The moment you click the button to RIDE, we search for the nearby drivers who will accept your ride. 
Suppose there are 15 drivers near your location, then we send the request to the first driver who is closest to you, then the second, and so on. 

There are a few conditions though, based on which we can not send the request to the nearby driver.
- Condition 1: If the driver rating is lower than 4. (out of 5)
- Condition 2: If you selected a specific car, and that car driver is not the closest one.

**In case there is no driver present as per your request for the car, we will ask you to select some other car.**
A table was given having drivers, cars model, their ratings, and distance from the customer, and using this data we have to provide the most appropriate car to the customer, with the calculated fare. 

Drivers:
ssn: 123, 'John Doe', rating: 4, distance: 50, carNumber: 'kl123'
ssn: 123, 'John Doe', rating: 4, distance: 50, carNumber: 'kl123'
ssn: 123, 'John Doe', rating: 4, distance: 50, carNumber: 'kl123'
ssn: 123, 'John Doe', rating: 4, distance: 50, carNumber: 'kl123'

Cars
number: 'kl123', model: 'Hynuday', color: blue
number: 'kl123', model: 'Hynuday', color: blue
number: 'kl123', model: 'Hynuday', color: blue
number: 'kl123', model: 'Hynuday', color: blue


---

classes:


RentRide:
 - Cars
 - Drivers



