const CHARGE_PER_KM = 8
const MIN_RATING = 4

let counter = 0

class RentRide {
  constructor({ drivers, cars, distanceToTravel, selectedCar }) {
    this.drivers = drivers
    this.cars = cars
    this.distanceToTravel = distanceToTravel
    this.cost = distanceToTravel * CHARGE_PER_KM
    this.selectedCar = selectedCar
  }

  sendDriverRequest(chosenDriver) {
    console.log(`sending request to driver: ${chosenDriver.getName()}`)
  }

  getCost() {
    return this.cost
  }

  bookRide() {
    if (this.selectedCar) {
      const chosenDriver = this.drivers.find(driver => driver.ssn == this.selectedCar.driverSsn)
      if (!chosenDriver) {
        return "Invalid driver chosen!!";
      }
      this.sendDriverRequest(chosenDriver)
      return this.cost
    }

    const filteredDrivers = this.drivers.filter(driver => driver.getRating() >= MIN_RATING)
    if (!filteredDrivers.length) {
      return "No driver is present close by!"
    }
    const sortedDrivers = filteredDrivers.sort((driverA, driverB) =>
      driverA.getDistance() - driverB.getDistance())
    this.sendDriverRequest(sortedDrivers[counter++])
    return this.getCost()
  }
}

module.exports = RentRide
