
const RentRide = require("./main")
const { Car } = require("./Services/Car")
const { Driver } = require("./Services/Driver")

const drivers = [
  new Driver({ name: 'John Doe', ssn: '1234', rating: 5, distance: 40 }),
  new Driver({ name: 'Mary Jane', ssn: '1235', rating: 4, distance: 50 }),
  new Driver({ name: 'Jack Black', ssn: '1236', rating: 4.4, distance: 20 }),
  new Driver({ name: 'Molly Joes', ssn: '1237', rating: 3.4, distance: 30 })
]

const cars = [
  new Car({ driverSsn: '1234', number: 'KL123', make: 'Hyundai', color: 'Blue' }),
  new Car({ driverSsn: '1235', number: 'JK123', make: 'Toyota', color: 'Blue' }),
  new Car({ driverSsn: '1236', number: 'ML133', make: 'Renault', color: 'Green' }),
  new Car({ driverSsn: '1237', number: 'BB234', make: 'Tata', color: 'Gray' }),
]

// eslint-disable-next-line no-undef
test('select new driver', () => {
  // eslint-disable-next-line no-undef
  expect(new RentRide({
    drivers, cars, distanceToTravel: 50,
    selectedCar: new Car({ driverSsn: '1235', number: 'JK123', make: 'Toyota', color: 'Blue' }),
  }).bookRide()).toEqual(400)
})

