
class Driver {
  constructor({ name = '', ssn = Math.random(), rating = 2, distance = 49 }) {
    this.name = name
    this.ssn = ssn
    this.rating = rating
    this.distance = distance
  }
  getDistance = () => this.distance
  getSsn = () => this.getSsn
  getName = () => this.name
  getRating = () => this.rating
}

module.exports = { Driver }