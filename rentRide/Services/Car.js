class Car {
  constructor({ number = '', make = '', color = '', carNo = '', driverSsn = '' }) {
    this.number = number
    this.make = make
    this.color = color
    this.carNo = carNo
    this.driverSsn = driverSsn
  }
}


module.exports = { Car }