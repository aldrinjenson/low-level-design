const services = {
  netflix: {name: "Netflix", price: 10, multiple: 10},
  amazon: {name: "Amazon Prime", price: 2, multiple: 5},
  hotstar: {name: "Hotstar", price: 1, multiple: 5},
}

const calculateCost = (streamingServices) =>{
  let cost  = 0

  for (const [key,val] of Object.entries(streamingServices)){
    const service = services[key]
    if (val % service.multiple !== 0){
      return `ERROR: ${service.name} allows viewing hours in multiples of ${service.multiple} only`
    }
    cost += (val / service.multiple) * service.price
  }
  return `Total amount to be paid: Rs. ${cost}`
}

console.log(calculateCost({netflix: 20, amazon: 10, hotstar:50}))
console.log(calculateCost({netflix: 10, amazon: 0, hotstar:100}))
console.log(calculateCost({netflix: 10, amazon: 2}))
